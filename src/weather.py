import requests
import traceback
from datetime import datetime, timezone, timedelta

from flask_babel import gettext

from env import OPENWEATHERMAP_API_KEY


class OpenWeatherMap:

    def __init__(self, logger, default_location_name: str = 'Minsk,by'):
        self.endpoint = 'http://api.openweathermap.org'
        self.default_location_name = default_location_name
        self.current_weather_url_pattern = f'{self.endpoint}/data/2.5/weather?units=metric&APPID={OPENWEATHERMAP_API_KEY}&q=%s'

        self.logger = logger
        self.time_format = '%H:%M:%S'

    def get_current_weather(self, location_name: str = None) -> str:
        """ Make request to check current weather. """

        location_name = location_name or self.default_location_name
        url = self.current_weather_url_pattern % location_name

        try:
            response = requests.post(url)
            out_str = self._parse_and_localize_current_weather_response(
                response)
        except Exception as e:
            err_msg = f'{type(e).__name__}: {e}'
            self.logger.error(err_msg)
            self.logger.error(traceback.format_exc())
            return err_msg

        return out_str

    def _parse_and_localize_current_weather_response(self, response) -> str:
        forecast = response.json()

        location = ', '.join([forecast['name'], forecast['sys']['country']])

        sunrise = forecast['sys']['sunrise']
        sunset = forecast['sys']['sunset']
        tz = timezone(timedelta(seconds=forecast['timezone']))  # forecast['timezone'] is a seconds offset from UTC
        sunrise_str = datetime.fromtimestamp(sunrise, tz=tz).strftime(self.time_format)
        sunset_str = datetime.fromtimestamp(sunset, tz=tz).strftime(self.time_format)

        # TODO: convert hPa to мм.рт.сл. for be and ru locales.
        # keep hPa for en locale.

        # f_<x> stands for "forecast" for <x>
        out_pat = (
            '{current_weather_for}: {location}\n\n'
            '{f_w1}, {f_w2}\n'
            '{t}: {f_t} °C\n'
            '{p}: {f_p} {p_units}\n'
            '{h}: {f_h}%\n'
            '{ws}: {f_ws} {w_units}, {wg}: {f_wg} {w_units}\n'
            '{sunrise}: {f_sunrise}\n'
            '{sunset}: {f_sunset}'
        )
        out = out_pat.format(
            current_weather_for=gettext('current_weather_for'), location=location,
            f_w1=forecast["weather"][0]["main"], f_w2=forecast["weather"][0]["description"],

            t=gettext("temperature"), f_t=forecast["main"]["temp"],
            p=gettext("pressure"), f_p=forecast["main"]["pressure"], p_units=gettext("hPa"),
            h=gettext("humidity"), f_h=forecast["main"]["humidity"],

            w_units=gettext("meter_sec"),
            ws=gettext("wind_speed"), f_ws=forecast["wind"]["speed"],
            wg=gettext("wind_gust"), f_wg=forecast["wind"]["gust"],

            sunrise=gettext("sunrise"), f_sunrise=sunrise_str,
            sunset=gettext("sunset"), f_sunset=sunset_str
        )
        return out
