# -*- coding: UTF-8 -*-

from flask import Flask, request, render_template, session
from flask_babel import Babel

import settings

from weather import OpenWeatherMap

app = Flask(__name__)
babel = Babel(app)

open_weather_map = OpenWeatherMap(logger=app.logger)

@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'be')


# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='sdfsdfsfdgdfg4fsd',
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


@app.route('/', methods=['GET', 'POST'])
def home():
    output_text = None

    if request.method == "POST":
        output_text = open_weather_map.get_current_weather()

    template = render_template('index.html', output_text=output_text)
    return template


if __name__ == '__main__':
    app.run(use_reloader=True, debug=True,
            host=settings.host, port=settings.port)
