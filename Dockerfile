FROM python:latest

COPY 'config/python' '/conf'

RUN pip install -r /conf/requirements.txt

COPY "src" "/srv"

WORKDIR "/srv"

ENTRYPOINT [ "python" ]

CMD [ "corpus.py" ]